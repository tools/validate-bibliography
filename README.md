# Validate bibliography

This shell script is intended to be included in the CI pipeline of the Project
and Young Research Group content repositories to validate the BibTeX files.

To run it in your project, create a file `.gitlab-ci.yml` and paste this code:

    validate bibliography:
      stage: test
      image: registry.gitlab.vgiscience.org/www/www.vgiscience.org:latest
      script:
        - curl -s https://gitlab.vgiscience.de/tools/validate-bibliography/-/raw/main/validate-bibliography.sh | bash

**Note:** This only tests whether a build using [`jekyll-scholar`][1] will pass.
For more sophisticated BibTeX validation see [this script][2].

[1]: https://rubygems.org/gems/jekyll-scholar
[2]: https://github.com/pezmc/biblatex-check
