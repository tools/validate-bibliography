#!/usr/bin/env bash

# exit on errors
set -e

# create function to be run at the end

do_build() {

    # create Gemfile with required gems
    echo -e \
        'source "https://rubygems.org"\n' \
        'gem "jekyll", "= 4.2"\n' \
        'gem "jekyll-scholar", "~> 7"\n' \
        >Gemfile

    # install required gems
    bundle install

    # create Jekyll configuration file with jekyll-scholar plugin
    echo -e \
        'plugins:\n' \
        '  - jekyll-scholar \n' \
        >_config.yml

    # prepend empty frontmatter to the readme file
    sed -i -e "1i ---\n---" README.md

    # append the liquid tag to have jekyll-scholar render the bibliography
    echo "{% bibliography --file references %}" >>README.md

    # build the jekyll page
    bundle exec jekyll build --quiet

}

# run function in one call for curl | bash safety
do_build
